from __future__ import print_function
import json
import logging
import os
import pprint

import google.auth.transport.grpc
import google.auth.transport.requests
import google.oauth2.credentials

from google.assistant.embedded.v1alpha2 import embedded_assistant_pb2
from google.assistant.library import Assistant as WrappedAssistant
from google.assistant.library.event import EventType

END_OF_UTTERANCE = embedded_assistant_pb2.AssistResponse.END_OF_UTTERANCE

api_endpoint = 'embeddedassistant.googleapis.com'
grpc_deadline = 60 * 5

class Assistant:
    def __init__(self, credentials, device_model_id):
        logging.info('init assistant')

        try:
            with open(credentials, 'r') as f:
                credentials = google.oauth2.credentials.Credentials(token=None,
                                                                    **json.load(f))
                http_request = google.auth.transport.requests.Request()
                credentials.refresh(http_request)
        except Exception as e:
            logging.error('Error loading credentials: %s', e)
            logging.error('Run google-oauthlib-tool to initialize '
                        'new OAuth 2.0 credentials.')
            sys.exit(-1)

        device_config = os.path.join(
                            os.path.expanduser('~/.config'),
                            'googlesamples-assistant',
                            'device_config_library.json'
                        )
        last_device_id = None
        try:
            with open(device_config) as f:
                device_config = json.load(f)
                device_model_id = device_config['model_id']
                last_device_id = device_config.get('last_device_id', None)
        except IOError:
            pass

        logging.info('device config loaded')

        self._assistant = WrappedAssistant(credentials, device_model_id)
        self._device_model_id = device_model_id

    def start(self, notify_exit_event):
        events = self._assistant.start()

        device_id = self._assistant.device_id
        print('device_model_id:', self._device_model_id)
        print('device_id:', device_id)

        for event in events:
            self.process_event(event)

            if notify_exit_event.is_set():
                break

    def process_event(self, event):
        """Pretty prints events.

        Prints all events that occur with two spaces between each new
        conversation and a single space between turns of a conversation.

        Args:
            event(event.Event): The current event to process.
        """
        if event.type == EventType.ON_CONVERSATION_TURN_STARTED:
            print()

        print(event)

        if (event.type == EventType.ON_CONVERSATION_TURN_FINISHED and
                event.args and not event.args['with_follow_on_turn']):
            print()
        if event.type == EventType.ON_DEVICE_ACTION:
            for command, params in event.actions:
                print('Do command', command, 'with params', str(params))

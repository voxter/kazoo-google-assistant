import logging
import os
import threading

import click

from assistant import Assistant

BLOCK_SIZE = 1024

def assistant_thread(event, device_model_id):
    logging.info('started assistant thread')

    assistant_credentials = os.path.join(click.get_app_dir('google-oauthlib-tool'), 'credentials.json')
    assistant = Assistant(assistant_credentials, device_model_id)

    logging.info('assistant ready')

    assistant.start(event)

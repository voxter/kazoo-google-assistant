#!/usr/bin/env python

import json
import requests
from string import Template

WEBSOCKET_PATH = "ws://gcp-api.voxter.com:5555"
# "ws://192.168.99.100:5555"

AUTH_TOKEN = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IjJkMTQwNDEzOTEyZjgzZjc1NDNkNWQ2MGY2NDM4MGMzIn0.eyJpc3MiOiJrYXpvbyIsImlkZW50aXR5X3NpZyI6IlJiLWU1b2k2Mzk0VDFFV3BaNmFfd0NrNDBpUGRCanJHdWFvZVdIZC1RbWciLCJhY2NvdW50X2lkIjoiNjZlZDMwYTFjZWI2ZWRlZTM1MmM3MTA1YzU3MzM3OTIiLCJvd25lcl9pZCI6ImJjOWM5NTg3YjQ5OGFhNDhkYWFhZmZhYzlkN2Q4YWJiIiwibWV0aG9kIjoiY2JfdXNlcl9hdXRoIiwiZXhwIjoxNTYzNDkwMzM1fQ.eyxIKbxP8jRkLy9XhLdsR0RuNUN_KbpQQIch5KW8wSv7bscYeEmqYcygLRaZR9A8RCQ5FkRCILkZarTfRrVFLWoDP1lMiEB8igC_8KDJXKrdjIO2CusR4gYiGMo5GiRNnD6RrEhTCLiz5Ak_5ujXblGpCprv7yAfMTN88YsRFIE6HFJ_aBGzRePyMswpiPdEbDIKAUxv3UhZpR01dmSzugzUbRRMot_pIPCa2Ln6uMIVIvitn4Cn2Rbr4Nqe9HFL-WgKPByqn08MChClNYI1L_iE991g-Sp7WtkRKN032Fok4ivwFZEcDBjXbVr_YECWyf66mMPwqXDWC7lN-wr5dg"
# "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6Ijc1MjZiODAzZTY1NDEyOWJhY2E3MDE1YzUxMDE5ZWNlIn0.eyJpc3MiOiJrYXpvbyIsImlkZW50aXR5X3NpZyI6IjlkQTRDYXkxOWItd2lTOUhOX3pLRHJVR3hHc3cxTEdoazZqdFpuX2FZUjgiLCJhY2NvdW50X2lkIjoiNjRlYjhiYzEyMjk0YmQ1ZDRmZjcwNGVkNDA5NjY0ZDEiLCJvd25lcl9pZCI6ImE1NDU0MzMwOTY3ZGYxYTA3YmYwMGVhNWJlNzAwZDU4IiwibWV0aG9kIjoiY2JfdXNlcl9hdXRoIiwiZXhwIjoxNTYwNjM1NDI4fQ.r-GSIacGwY-PD4xIEUMnNk6QSCVa-gcMoBZg5aDOSnE847qmGn1YbxbxUtzfMGteRdabS5kIflaaJopR0j9lJVBCJyY9l-mf_T7wcFx3ETHUTYeVe7Ny1aNBZrOWRdQPqaRH1K6V3WemMbPPeI5P9o1ZDUAsFfNff9KVykYFKDkGYoL4YULE_ULw_qMb5243b_GQ9m8rTC5MFkhd3m3v9hOxel4Ye6xiprrxWbjckG8ZNirgtMPc0C9_W93WM5aLac6fgpWrSF_3KMGRakzji-HikU-qg4CDGlghbv5rH7VVjEW4rSCzGahKsjZtPlUbUW0F_TnVvQQ06xhFZWsX5g"

ACCOUNT_ID = "043a546e37a2e2caa151f91c50d701f5"
# "64eb8bc12294bd5d4ff704ed409664d1"

EAVESDROP_URI = 'https://gcp-api.voxter.com:8443/v1/accounts/{accountId}/queues/eavesdrop'
# 'http://192.168.99.100:8000/v1/accounts/{accountId}/queues/eavesdrop'

USER_PROFILE_URI = 'https://gcp-api.voxter.com:8443/v1/accounts/{accountId}/users/{userId}''
# 'http://192.168.99.100:8000/v1/accounts/{accountId}/users/{userId}'

EAVESDROP_DEVICE_ID = '4091784df0a99c3c2cadc3d53f24cf38'
# '118c38bcabe9b0fcdd6fa510bfbd5ece'

import websocket
try:
    import thread
except ImportError:
    import _thread as thread
import time

def on_message(ws, message):
    AA = json.loads(message)
    print message
    if (AA.get('name') == "CHANNEL_BRIDGE"):
        # print message
        callId = AA.get('data', {}).get('call_id', {})
        customChannelVars = AA.get('data', {}).get('custom_channel_vars', {})

        deviceId = customChannelVars.get('authorizing_id')
        accountId = customChannelVars.get('account_id')
        ownerId = customChannelVars.get('owner_id')

        print('Device: %s', deviceId)
        print('Call: %s', callId)
        print('User: %s', ownerId)

        enableAssistant = check_user_pref(ownerId, accountId)

        print('Assistants: %s', enableAssistant)
        if (enableAssistant == 'true'):
            call_evesdrop(callId, accountId, deviceId)


def check_user_pref(userId, accountId):
    getUserUri = USER_PROFILE_URI
    headers = {
        "X-Auth-Token": AUTH_TOKEN,
        "Content-Type": "application/json",
        "Accept": "application/json, text/javascript, */*; q=0.01"
    }
    res = requests.get(url = getUserUri.format(accountId = accountId, userId = userId), headers = headers)
    response = json.loads(res.text)
    return response.get('data').get('google_assistant')

def call_evesdrop(callId, accountId, deviceId):
    data = {"data": {
        "call_id": callId,
        "id": EAVESDROP_DEVICE_ID,
        "mode": "full"
    }}
    headers = {
        "X-Auth-Token": AUTH_TOKEN,
        "Content-Type": "application/json",
        "Accept": "application/json, text/javascript, */*; q=0.01"
    }
    r = requests.put(url = EAVESDROP_URI.format(accountId = accountId), data = json.dumps(data), headers = headers)

    print r
    print r.text

def on_error(ws, error):
    print(error)

def on_close(ws):
    print("### closed ###")

def on_open(ws):

    def run(*args):
        payload = {
            "action": "subscribe",
            "auth_token": AUTH_TOKEN,
            "data": {
                "account_id": ACCOUNT_ID,
                "binding": "call.*.*"
            }
        }

        ws.send(json.dumps(payload))

    thread.start_new_thread(run, ())


if __name__ == "__main__":
    websocket.enableTrace(True)
    ws = websocket.WebSocketApp(WEBSOCKET_PATH,
                              on_message = on_message,
                              on_error = on_error,
                              on_close = on_close)
    ws.on_open = on_open
    ws.run_forever()
import concurrent.futures
import logging
import os
import threading

import click

from assistant_thread import assistant_thread

@click.command()
@click.option('--device-model-id', required=True,
              metavar='<device model id>',
              help='Unique device model identifier.')
def main(device_model_id):
    # Setup logging.
    logging.basicConfig(level=logging.DEBUG)

    event = threading.Event()
    with concurrent.futures.ThreadPoolExecutor() as executor:
        logging.info('started thread pool')
        executor.submit(assistant_thread, event, device_model_id)
        event.wait()

    logging.info('going down')

if __name__ == '__main__':
    main()
